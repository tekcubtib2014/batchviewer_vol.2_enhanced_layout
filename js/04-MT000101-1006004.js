
var contentConstant = {
    "CONTENT_PATH": '04-MT000101-1006004/',
    "BACKGROUND_IMAGE" : 'char_background.png',
    "Margin": 20,
    "ScaleWidth" : 0,
    "ScaleHeight" : 0,
    "LeftPaddingScale" : 0,
    "TopPaddingScale":  0,
    "ImageWidth": 740,
    "ImageHeight": 500
};

var CONTENTS_INFO = [
     {btn: 'BTN_BOTTOM_MIDDLE_FIRST',   imgId: '01_01'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SECOND',  imgId: '01_02'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_THIRD',   imgId: '01_03'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_FORTH',   imgId: '01_04'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_FIFTH',   imgId: '01_05'}

    ,{btn: 'BTN_BOTTOM_FIRST_COUNT',    imgId: '02_01'}
    ,{btn: 'BTN_BOTTOM_SECOND_COUNT',   imgId: '02_02'}
    ,{btn: 'BTN_BOTTOM_THIRD_COUNT',    imgId: '02_03'}
    ,{btn: 'BTN_BOTTOM_FORTH_COUNT',    imgId: '02_04'}
    ,{btn: 'BTN_BOTTOM_FIFTH_COUNT',    imgId: '02_05'}
    ,{btn: 'BTN_BOTTOM_SIXTH_COUNT',    imgId: '02_06'}
    ,{btn: 'BTN_BOTTOM_SEVENTH_COUNT',  imgId: '02_07'}
    ,{btn: 'BTN_BOTTOM_EIGHTH_COUNT',   imgId: '02_08'}
    ,{btn: 'BTN_BOTTOM_NINTH_COUNT',    imgId: '02_09'}

    ,{btn: 'none',                      imgId: 'img'}
    ,{btn: 'BTN_BOTTOM_ALL',            imgId: 'img3'}
];

var GRAPH_INFO = [
    'img','img3','01_01','01_02','01_03','01_04','01_05','02_01','02_02','02_03','02_04','02_05','02_06','02_07','02_08','02_09'
];

var scaleFactor;
var flagZoom = false;

var imageTop;
var imageLeft;
var position;
var baseImg = "#img1";

var title;
var pageURL;
var contentID;



$(document).ready(function() {

    initBatchViewer.call(this);

});

