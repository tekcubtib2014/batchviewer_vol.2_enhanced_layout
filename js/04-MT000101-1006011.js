
var CONTENTS_INFO = [
    {btn: 'BTN_BOTTOM_MIDDLE_FIRST',    imgId: ['01_01','01_02','01_03']}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SECOND',  imgId: '02_01'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_THIRD',   imgId: '03_01'}

    ,{btn: 'none',                      imgId: '01_02'}
    ,{btn: 'none',                      imgId: '01_03'}
    ,{btn: 'none',                      imgId: 'img'}
    ,{btn: 'none',                      imgId: 'img2'}
    ,{btn: 'BTN_BOTTOM_ALL',            imgId: 'img3'}
];

var GRAPH_INFO = ['img', 'img2', 'img3', '01_01', '01_02', '01_03', '02_01', '03_01'];

var scaleFactor;
var flagZoom = false;

var imageTop;
var imageLeft;
var position;
var baseImg = "#img1";

var title;
var pageURL;
var contentID;

$(document).ready(function() {
    initBatchViewer.call(this);
});