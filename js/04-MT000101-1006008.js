

//
// 레이아웃 수정 전 설정했던 좌표정보. [jgkim] 2015.04.09
//
var COORDS_INFO = [
     {id:'area01', coords : [18,15,400,375],    cutImage: "cut_P01"}
    ,{id:'area02', coords : [78,408,465,601],    cutImage: "cut_P02"}
    ,{id:'area03', coords : [321,930,683,1070],    cutImage: "cut_P03"}
    ,{id:'area04', coords : [423,158,658,375],    cutImage: "cut_P04"}
    ,{id:'area05', coords : [499,477,873,626],    cutImage: "cut_P05"}
    ,{id:'area06', coords : [750,227,1153,442],    cutImage: "cut_P06"}
    ,{id:'area07', coords : [925,885,1410,1071],    cutImage: "cut_P07"}
    ,{id:'area08', coords : [1062,9,1257,187],    cutImage: "cut_P08"}
    ,{id:'area09', coords : [1122,595,1587,841],    cutImage: "cut_P09"}
    ,{id:'area10', coords : [1293,60,1586,133],    cutImage: "cut_P10"}
    ,{id:'area11', coords : [1193,235,1476,416],    cutImage: "cut_P11"}
];

var title;
var scaleFactor;
var flagZoom = false;
var rate;
var currentURL = $(location).attr('pathname');
var imgPath = currentURL.substring(currentURL.lastIndexOf("/"));
imgPath = "../img/" + imgPath.substring(0, imgPath.lastIndexOf("."));

var Agent = navigator.userAgent;
var is_safari = Agent.indexOf("Safari") > -1;
var is_iPad = Agent.match(/iPad/i) != null;

var baseImg = "#img";

$(document).ready(function() {
   initViewer.call(this);
});

