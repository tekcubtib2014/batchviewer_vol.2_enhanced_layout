
var title;
var scaleFactor;
var flagZoom = false;
var rate;
var rateCoords;
var currentURL = $(location).attr('pathname');
var imgPath = currentURL.substring(currentURL.lastIndexOf("/"));
imgPath = "../img/" + imgPath.substring(0, imgPath.lastIndexOf("."));

var Agent = navigator.userAgent;
var is_safari = Agent.indexOf("Safari") > -1;
var is_iPad = Agent.match(/iPad/i) != null;
var isShowZoomArea;

var isLockScreen = false;

var baseImg = "#img";
var mask = "#mask";

function initLogger() {
    LOGGER = logger.getInstance();
    window.requestFileSystem(LocalFileSytem.PERSISTENT, 0, function(fileSystem) {
        fileSystem.root.getFile(Enum.Const.LICENSE_FLAG, {create: false, exclusive: false}, function() {
            contentsArray.enabledContents = fullContentsArray.enabledContents;
        })
    });
}

function handleOpenURL (url) {
    setTimeout(function() {
        openUrlInContents(url);
    }, 100);
}

function initViewer () {

    if ( is_iPad ) {
        var iPadArr = [
            '../../../js/plugins/handleOpenURL.js',
            '../../../js/plugins/LaunchMyApp.js'
        ];
        addiPadJS(iPadArr);
    }

    document.addEventListener('deviceready', initLogger, false);

    console.log($("#img").width());
    setTitle();

    $(baseImg).hide();

    setTimeout(function () {
        load(baseImg, flagZoom);
    }, 250);

    //load(baseImg, flagZoom);

    setTimeout(function () {
        $(baseImg).show();
    }, 250);

    $(window).resize(function() {
        load(baseImg, flagZoom);
    });

    setTimeout(function () {
        $(mask).hide();
        $(mask).css({
            "opacity" : 0
        });
    },150);

    /**
     * [jgkim] 2015.04.15
     * 이미지 조정 시작
     * 아이패드에서 딜레이 필요(확인 중)
     * @param baseImg
     * @param flagZoom
     */
    function load(baseImg, flagZoom) {
        for (var i = 0; i < COORDS_INFO.length; i++) {
            var img = "#" + COORDS_INFO[i].cutImage;
            sizeToFit("#container",img,false);
            //console.log(img);
        }

        if (flagZoom == true) {
            sizeToFit("#container", baseImg, false);
            //sizeToFit("#container", "#img4", false);
            $("#img4").css({
                width: $(baseImg).width(),
                height:  $(baseImg).height(),
                position: "absolute",
                top: $(baseImg).offsetTop,
                left: $(baseImg).offsetLeft
            });
        } else {
            sizeToFit("#container", baseImg, true);
            //sizeToFit("#container", "#img4", true);
            $("#img4").css({
                width: $(baseImg).width(),
                height:  $(baseImg).height()
                //position: "absolute",
                //top: $(baseImg).offsetTop,
                //left: $(baseImg).offsetLeft
            });
        }
    }

    /**
     * [jgkim] 2015.04.15
     * 이미지 리사이즈 주 로직.
     * @param source
     * @param dest
     * @param isSet
     */
    function sizeToFit(source, dest, isSet) {

        var marginH = 50;                           // margin height

        var tWidth = $(source).width();             // target width
        var tHeight = $(window).height();           // target height

        var bottomHeight = $("#bottom").height();
        tHeight -= bottomHeight + marginH;

        var sWidth = $(dest)[0].naturalWidth;
        var sHeight = $(dest)[0].naturalHeight;

        var rateWidth = tWidth / sWidth;
        var rateHeight = tHeight / sHeight;

        console.log(sWidth + " // " + sHeight);

        rate = rateWidth < rateHeight ? rateWidth : rateHeight;

        rateCoords = rate;

        var width = sWidth * rate;
        var height = sHeight * rate;

        $(dest).css("width", width);
        $(dest).attr("height", height);

        if (isSet == true)
            scaleFactor = rate;
    }

    function sizeToFitForUnitView(source, dest, isSet) {

        var marginH = 50;                           // margin height

        var tWidth = $(source).width();             // target width
        var tHeight = $(window).height();           // target height

        var bottomHeight = $("#bottom").height();
        tHeight -= bottomHeight + marginH;

        var sWidth = $(dest)[0].naturalWidth;
        var sHeight = $(dest)[0].naturalHeight;

        var rateWidth = tWidth / sWidth;
        var rateHeight = tHeight / sHeight;

        rate = rateWidth < rateHeight ? rateWidth : rateHeight;

        var width = sWidth * rate;
        var height = sHeight * rate;

        $(dest).css("width", width);
        $(dest).attr("height", height);

        if (isSet == true)
            scaleFactor = rate;
    }

    $('#dialog').css({
        left: "150px",
        top: "100px"
    });

    /*
     화면 잠금용 디비전 css
     */
    $(mask).css({
        width: $(window).width() - 150,
        height: $(window).height(),
        position: "absolute",
        top: 0,
        left: "150px",
        //opacity: 0,
        backgroundColor: "white",
        'z-index': 999
        //display: 'none'
    });

    /**
     * 클릭 이벤트 처리
     */
    $("#container").click(function() {
        imgClick(event);
    });

    /**
     * 클릭 이벤트 처리
     * @param e
     */
    function imgClick(e){

        for (var i = 0; i < COORDS_INFO.length; i++) {
            var img = "#" + COORDS_INFO[i].cutImage;
            sizeToFitForUnitView("#container",img,false);
            //console.log(img);
        }

        if (flagZoom == true){
            if (isShowZoomArea == true) {
                $("#img4").show();
            } else {
                $("#img").show();
            }

            $(".unitView").hide();
            flagZoom = false;
            return;
        }
        var x = e.offsetX;
        var y = e.offsetY;

        for (var j = 0; j < COORDS_INFO.length; j++){
            if (contains(x, y, COORDS_INFO[j].coords) == true){
                var test = "#" + COORDS_INFO[j].cutImage;
                $(".baseImages").hide();
                $(test).show();
                flagZoom = true;
                break;
            }
        }
    }

    /**
     * 클릭 지점이 확대 영역에 포함되는 지 여부 판별
     * @param x
     * @param y
     * @param rect
     * @returns {boolean}
     */
    function contains(x, y, rect){

        var isContains = false;

        var x1 = rect[0] * rateCoords;
        var y1 = rect[1] * rateCoords;
        var x2 = rect[2] * rateCoords;
        var y2 = rect[3] * rateCoords;

        //console.log("clicked here 2 : "  + x1 + " / " + y1 + " / " + x2 + " / " + y2);

        //if (x < x1 || x > x2)
        //    isContains = false;
        //if (y < y1 || y > y2)
        //    isContains = false;

        if (x > x1 && x < x2) {
            if (y > y1 && y < y2) {
                isContains = true;
            }
        }

        console.log(isContains);
        return isContains;
    }

    /**
     * 타이틀 토글
     *
     */
    $("#btnToggleTitle").click(function() {
        //toggleTitle.call(this);
        //$("#btnToggleTitle").attr("height", 250);
        this.src = getToggleImageName(this.src);
        var titleTag = $('#titleArea');
        if(titleTag.text().length > 1) {
            titleTag.text('');
        } else {
            titleTag.text(title);
        }
    });

    /**
     * 확대 영역 표시
     *
     */
    $("#btnToggleZoomPoint").click(function() {
        if (isLockScreen == false) {
            this.src = getToggleImageName(this.src);
            if (this.src.indexOf('toggle') > 0) {
                //$("#img").attr("src",imgPath + "/background4.png");
                $("#img").hide();
                $("#img4").show();
                isShowZoomArea = true;
            } else if (this.src.indexOf('toggle') == -1) {
                //$("#img").attr("src",imgPath + "/background.png");
                $("#img4").hide();
                $("#img").show();
                isShowZoomArea = false;
            }
            $(".unitView").hide();
            flagZoom = false;
        }
    });

    /**
     * 화면 잠금 토글
     *
     */
    $("#btnLockScreen").click(function() {
        this.src = getToggleImageName(this.src);
        if (this.src.indexOf('toggle') > 0) {
            $(mask).hide();
            isLockScreen = false;
        } else if (this.src.indexOf('toggle') == -1) {
            $(mask).show();
            isLockScreen = true;
        }
    });
}

/**
 * [jgkim] 2015.04.22
 * 타이틀 설정
 *
 */
function setTitle() {
    pageURL = this.location.href.substring(this.location.href.lastIndexOf('/') + 1);
    contentID = pageURL.substring(0, pageURL.lastIndexOf('.'));

    for (var i = 0; i < fullContentsArray.enabledContents.length; i++) {
        if (contentID === fullContentsArray.enabledContents[i].cd) {
            title = fullContentsArray.enabledContents[i].tit;
            break;
        }
    }
    $("title").html(title);
    $("#titleArea").html(title);
    $("#container img").css('border', 'none');
}

