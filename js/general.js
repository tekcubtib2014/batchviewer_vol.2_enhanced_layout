
var title;
var scaleFactor;
var flagZoom = false;
var rate;
var rateCoords;
var currentURL = $(location).attr('pathname');
var imgPath = currentURL.substring(currentURL.lastIndexOf("/"));
imgPath = "../img/" + imgPath.substring(0, imgPath.lastIndexOf("."));

var Agent = navigator.userAgent;
var is_safari = Agent.indexOf("Safari") > -1;
var is_iPad = Agent.match(/iPad/i) != null;

var baseImg = "#img1";

var clickCount = 0;
currentURL = currentURL.substring(currentURL.lastIndexOf("/"));

function initLogger() {
    LOGGER = logger.getInstance();
    window.requestFileSystem(LocalFileSytem.PERSISTENT, 0, function(fileSystem) {
        fileSystem.root.getFile(Enum.Const.LICENSE_FLAG, {create: false, exclusive: false}, function() {
            contentsArray.enabledContents = fullContentsArray.enabledContents;
        })
    });
}

function handleOpenURL (url) {
    setTimeout(function() {
        openUrlInContents(url);
    }, 100);
}

function initBatchViewer() {

    if ( is_iPad ) {
        var iPadArr = [
            '../../../js/plugins/handleOpenURL.js',
            '../../../js/plugins/LaunchMyApp.js'
        ];
        addiPadJS(iPadArr);
    }

    document.addEventListener('deviceready', initLogger, false);

    var dialog = $('#dialog');

    $("#mainTable").on("dragstart", function (event) {
        return false;
    });
    $(window.document).on("selectstart", function (event) {
        return false;
    });
    $(window.document).on("contextmenu", function (event) {
        return false;
    });

    $("#img").hide();

    setTitle.call(this);

    sizeToFit('#container', baseImg, true);

    setTimeout(function () {
        load.call(this);
    },250);

    setTimeout(function () {
        $("#img").show();
    },250);

    setDialog.call(this);
    bindClickEvent.call(this);

    $("#dialog").hide();

    $(document).bind('touchstart', function (event) {
        $('#dialog').draggable({
            containment: "#container",
            iframeFix: true,
            revert: 'invalid',
            stop: function (event, ui) {
                $(this).draggable('option', 'revert', 'invalid');
            },
            zIndex: 998
        });
        $("#container").droppable({});
    });

    $(window).resize(function () {

        sizeToFit('#container', baseImg, true);

        setTimeout(function() {
            load.call(this);
        }, 150);

        dialog.css({
            left: "180px",
            top: "150px"
        });

        $('#dialogBundleView').css({
            left: "150px",
            top: "70px",
            "z-index" : "900"
        });

        $('#dialogUnitView').css({
            left: "150px",
            top: "250px",
            "z-index" : "900"
        });
    });

    $("#dialogUnitView").mousedown(function() {
        $("#dialogUnitView").css("z-index", 901);
        $("#dialogBundleView").css("z-index", 900);

    });

    $("#dialogBundleView").mousedown(function() {
        $("#dialogBundleView").css("z-index", 901);
        $("#dialogUnitView").css("z-index", 900);
    });

    if (is_iPad) {
        $(document).bind('touchstart', function (event) {
            $('#dialog').draggable({
                containment: "#container",
                iframeFix: true,
                revert: 'invalid',
                stop: function (event, ui) {
                    $(this).draggable('option', 'revert', 'invalid');
                },
                zIndex: 998
            });
            $("#container").droppable({});
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    function setTitle() {
        pageURL = this.location.href.substring(this.location.href.lastIndexOf('/') + 1);
        contentID = pageURL.substring(0, pageURL.lastIndexOf('.'));

        for (var i = 0; i < fullContentsArray.enabledContents.length; i++) {
            if (contentID === fullContentsArray.enabledContents[i].cd) {
                title = fullContentsArray.enabledContents[i].tit;
                break;
            }
        }
        $("title").html(title);
        $("#titleArea").html(title);
    }

    function setDialog() {

        dialog.show();

        dialog.draggable({
            containment: "#container",
            iframeFix: true,
            revert: 'invalid',
            stop: function (event, ui) {
                $(this).draggable('option', 'revert', 'invalid');
            },
            zIndex: 998
        });

        $("#container").droppable({});

        dialog.css({
            left: "180px",
            top: "150px",
            "background-color": "#FDEADA",
            "opacity": 1,
            "border": "2px solid #FF6699",
            "z-index": "998",
            "position": "absolute",
            "border-radius": "10px"
        });

        $('#showDialog').css({
            "width": "126px",
            "max-width": "100%",
            "display": "block",
            "margin-left": "-12px",
            "margin-top": "50px",
            "position": "relative"
        });

        $(".graphImage").hide();
        $("#img3").hide();

        $('.dialog').draggable({
            containment: "#container",
            iframeFix: true,
            zIndex: 998,
            revert: 'invalid',
            stop : function (event, ui) {
                $(this).draggable('option', 'revert', 'invalid');
            }
        });

        $('#dialogBundleView').css({
            left: "150px",
            top: "70px",
            "z-index" : "900"
        });

        $('#dialogUnitView').css({
            left: "150px",
            top: "250px",
            "z-index" : "900"
        });
    }

    function bindClickEvent() {

        $("#BTN_BOTTOM_ALL").click(function () {
            toggleEntireView.call(this);
        });

        $("#BTN_BOTTOM_MODE").click(function () {
            toggleBtnMode.call(this);
        });

        $('.btnUnitView').click(function () {
            toggleBundleOrUnitView.call(this);
        });

        //$('#btnCloseDialog').click(function () {
        //    $('#dialog').hide();
        //    $('#showDialog')[0].src = getToggleImageName($('#showDialog')[0].src);
        //});

        $('.btnCloseDialog').click(function () {
            $(this).parents(".dialog").hide();
            if (this.id == "btnCloseDialog") {
                $('#showDialog')[0].src = getToggleImageName($('#showDialog')[0].src);
            } else if (this.id == "btnCloseBundleViewDialog") {
                $('#showDialogBundleView')[0].src = getToggleImageName($('#showDialogBundleView')[0].src);
            } else if (this.id == "btnCloseUnitViewDialog") {
                $('#showDialogUnitView')[0].src = getToggleImageName($('#showDialogUnitView')[0].src);
            }
        });

        $('#showDialog').click(function () {
            bindDialogClickEvent('#showDialog');
            //if (this.src.indexOf('toggle') > 0) {
            //    $('#dialog').hide();
            //} else if (this.src.indexOf('toggle') == -1) {
            //    $('#dialog').show();
            //}
            //$(this)[0].src = getToggleImageName($(this)[0].src);
        });

        $('#showDialogBundleView').click(function () {
            bindDialogClickEvent('#showDialogBundleView');
        });

        $('#showDialogUnitView').click(function () {
            bindDialogClickEvent('#showDialogUnitView');
        });

        function bindDialogClickEvent(btn) {
            var clickedBtn = $(btn);
            var dialogThis = $(btn.replace("showD", "d"));
            if (clickedBtn[0].src.indexOf('toggle') > 0) {
                dialogThis.hide();
            } else if (clickedBtn[0].src.indexOf('toggle') == -1) {
                dialogThis.show();
            }
            clickedBtn[0].src = getToggleImageName(clickedBtn[0].src);
        }

        $("#btnToggleTitle").click(function () {
            toggleTitle.call(this);
        });

        $("#btnLockScreen").click(function () {
            this.src = getToggleImageName(this.src);
            if (this.src.indexOf('toggle') > 0) {
                $("#mask").hide();
                $(".hideDialog").each(function() {
                    $(this).show();
                    $(this).removeClass("hideDialog");
                });
                $('#showDialog').click(function () {
                    bindDialogClickEvent('#showDialog');
                });

                $('#showDialogBundleView').click(function () {
                    bindDialogClickEvent('#showDialogBundleView');
                });

                $('#showDialogUnitView').click(function () {
                    bindDialogClickEvent('#showDialogUnitView');
                });
            } else if (this.src.indexOf('toggle') == -1) {
                $("#mask").show();
                $(".dialog").each(function() {
                    if($(this).css("display") != "none") {
                        $(this).addClass("hideDialog");
                        $(this).hide();
                    }
                });
                $("#showDialogBundleView").unbind("click");
                $("#showDialogUnitView").unbind("click");
                $("#showDialog").unbind("click");
            }
        });

        $(".bottomBtn").css('width', '100px');          // 보기 버튼
        $("#BTN_BOTTOM_MODE").css('width', '200px');    // 모드 버트
        $(".titleOn").css("width", '150px');            // 타이틀 버튼
    }

    function sizeToFit(source, dest, isSet) {

        var marginH = 50;   // margin height

        var tWidth = $(source).width();             // target width
        var tHeight = $(window).height();           // target height

        var bottomHeight = $("#bottom").height();
        tHeight -= bottomHeight + marginH;

        var sWidth = $(dest)[0].naturalWidth;
        var sHeight = $(dest)[0].naturalHeight;

        var rateWidth = tWidth / sWidth;
        var rateHeight = tHeight / sHeight;

        var rate = rateWidth < rateHeight ? rateWidth : rateHeight;
        var width = sWidth * rate;
        var height = sHeight * rate;

        $(dest).attr("width", width + "px");
        $(dest).attr("height", height + "px");

        if (isSet == true)
            scaleFactor = rate;
    }

    function load() {

        for (var i = 0; i < GRAPH_INFO.length; i++) {
            var graphImg = "#" + GRAPH_INFO[i];

            var baseWidth = $(baseImg).css('width');
            var baseHeight = $(baseImg).css('height');

            $(graphImg).css('width', baseWidth);
            $(graphImg).css('height', baseHeight);

            var position = $(baseImg).offset();

            var imgLeft = position.left;
            var imgTop = position.top;

            $(graphImg).css({
                position: "absolute",
                left: imgLeft,
                top: imgTop
            });

            $('.graphBaseImages').css({
                position: "absolute",
                left: imgLeft,
                top: imgTop
            });
        }

        $(".bottomBtn").css('min-width', "80px");
        $(".bottomBtn").css('width', $(window).width() * 0.06);
        $("#BTN_BOTTOM_MODE").css('width', $(window).width() * 0.12);
        $("#BTN_BOTTOM_MODE").css('width', $(window).width() * 0.12);
        $(".titleOn").css('width', $(window).width() * 0.09);
    }

    function toggleTitle() {
        this.src = getToggleImageName(this.src);
        var titleTag = $('#titleArea');
        if (titleTag.text().length > 1) {
            titleTag.text('');
        } else {
            titleTag.text(title);
        }
    }

    function toggleEntireView() {
        if (this.src.indexOf('toggle') == -1) {
            $('#img3').hide();
        } else {
            initializeGraphImgs();
            if($('#BTN_BOTTOM_MODE').length > 0) {
                $('#BTN_BOTTOM_MODE')[0].src = getToggleOnImageName($('#BTN_BOTTOM_MODE')[0].src);
            }
            $('#img3').show();
        }
        $(this)[0].src = getToggleImageName($(this)[0].src);
        clickCount = 0;
    }

    function toggleBtnMode() {
        initializeGraphImgs();
        $('#img3').hide();
        $(this)[0].src = getToggleImageName($(this)[0].src);
        $("#BTN_BOTTOM_ALL")[0].src = getToggleOffImageName($("#BTN_BOTTOM_ALL")[0].src);
    }

    function extractedFunctionFor6021(img) {
        if (this.src.indexOf('toggle') > 0) {
            $(img).hide();
        } else if (this.src.indexOf('toggle') == -1) {
            $('#img3').hide();
            $('.unitView').hide();
            $('.btnUnitViewEX').each(function () {
                $(this)[0].src = getToggleOnImageName($(this)[0].src);
            });
            $(img).show();
        }
    }

    function toggleOFFTargetButton(target) {
        $(target)[0].src = getToggleOffImageName($(target)[0].src);
    }

    function toggleONTargetButton(target) {
        $(target)[0].src = getToggleOnImageName($(target)[0].src);
    }

    function toggleBundleOrUnitView() {
        var id = this.id;
        if($('#BTN_BOTTOM_MODE').length > 0) {
            if ($('#BTN_BOTTOM_MODE')[0].src.indexOf('toggle') == -1) {
                for (var i = 0; i < CONTENTS_INFO.length; i++) {
                    if (id == CONTENTS_INFO[i].btn) {
                        var img = "#" + CONTENTS_INFO[i].imgId;
                        if (this.src.indexOf('toggle') > 0) {
                            $(img).hide();
                        } else if (this.src.indexOf('toggle') == -1) {
                            $('#img3').hide();
                            $(img).show();
                        }
                    }
                }
                //$("#BTN_BOTTOM_ALL")[0].src = getToggleOffImageName($("#BTN_BOTTOM_ALL")[0].src);
                toggleOFFTargetButton("#BTN_BOTTOM_ALL");
                $(this)[0].src = getToggleImageName($(this)[0].src);
            } else {
                for (var i = 0; i < CONTENTS_INFO_2.length; i++) {
                    if (id == CONTENTS_INFO_2[i].btn) {
                        var imgArray = CONTENTS_INFO_2[i].images;
                        var img = imgArray.shift();
                        //console.log(img);
                        imgArray.push(img);

                        if (img == "EMPTY") {
                            for (var j = 0; j < imgArray.length; j++) {
                                var imgHided = "#" + imgArray[j];
                                $(imgHided).hide();
                            }
                        } else {
                            //$('#' + img).show();
							var imgToHide = imgArray[imgArray.indexOf(img) - 1];
                            $('#' + img).show();
                            //$("#" + imgToHide).hide();
                            setTimeout(function() {
                                $("#" + imgToHide).hide();
                            },150);
                            //$('#' + img).show();
                        }
                    }
                }
            }
        }else {
            for (var i = 0; i < CONTENTS_INFO.length; i++) {
                if (id == CONTENTS_INFO[i].btn) {
                    if($.isArray(CONTENTS_INFO[i].imgId)) {
                        $('#img3').hide();
                        var imgArray = CONTENTS_INFO[i].imgId;
                        img = "#" + imgArray[clickCount];
                        $(img).show();
                        clickCount++;
                        if(clickCount == imgArray.length + 1) {
                            clickCount = 0;
                            for(var i = 0; i < imgArray.length; i++) {
                                var imgInit = "#" + imgArray[i];
                                $(imgInit).hide();
                            }
                        } else if (img == "#none") {
                            for(var i = 0; i < imgArray.length; i++) {
                                var imgInit = "#" + imgArray[i];
                                $(imgInit).hide();
                            }
                        }
                    }else if(currentURL == "/04-MT000101-1006021.html") {
                        var img = "#" + CONTENTS_INFO[i].imgId;
                        switch (CONTENTS_INFO[i].btn) {
                            case "BTN_BOTTOM_MIDDLE_FIRST" :
                            case ("BTN_BOTTOM_MIDDLE_SECOND") :
                                extractedFunctionFor6021.call(this, img);
                                break;
                            default :
                                if (this.src.indexOf('toggle') > 0) {
                                    $(img).hide();
                                } else if (this.src.indexOf('toggle') == -1) {
                                    $('#img3').hide();
                                    $('.bundleView').hide();
                                    $(img).show();
                                    $('.btnBundleView').each(function() {
                                        $(this)[0].src = getToggleOnImageName($(this)[0].src);
                                    });
                                }
                        }
                        $(this)[0].src = getToggleImageName($(this)[0].src);

                    }else {
                        var img = "#" + CONTENTS_INFO[i].imgId;
                        if (currentURL == "/04-MT000101-1006006.html") {
                            $(img).toggle();
                            console.log(this.id);
                            if (this.id == 'BTN_BOTTOM_MIDDLE_SECOND') {
                                $(this)[0].src = getToggleImageName($(this)[0].src);
                            }
                            if($("#img3").css('display') == 'block') {
                                $("#img3").hide();
                            }
                        } else if (this.src.indexOf('toggle') > 0) {
                            $(img).hide();
                        } else if (this.src.indexOf('toggle') == -1) {
                            $('#img3').hide();
                            $(img).show();
                        }
                        $(this)[0].src = getToggleImageName($(this)[0].src);

                    }
                }
            }
            toggleOFFTargetButton("#BTN_BOTTOM_ALL");
        }
    }

    function extractedFunctionFor6021(img) {
        if (this.src.indexOf('toggle') > 0) {
            $(img).hide();
        } else if (this.src.indexOf('toggle') == -1) {
            $('#img3').hide();
            $('.unitView').hide();
            $('.btnUnitViewEX').each(function () {
                $(this)[0].src = getToggleOnImageName($(this)[0].src);
            });
            $(img).show();
        }
    }

    function initializeGraphImgs() {
        $('.unitView').hide();
        $('.bundleView').hide();
        $('.btnUnitView').each(function() {
            $(this)[0].src = getToggleOnImageName($(this)[0].src);
        });
        if($('#BTN_BOTTOM_MODE').length > 0) {
            CONTENTS_INFO_2 = JSON.parse(JSON.stringify(oCONTENTS_INFO_2));
        }
        clickCount = 0;
    }
}
