

//
// 레이아웃 수정 전 설정했던 좌표정보. [jgkim] 2015.04.09
//
var COORDS_INFO = [
     {id:'area01', coords : [112,308,205,386],   cutImage: "cut_P01"}
    ,{id:'area02', coords : [11,411,237,459],    cutImage: "cut_P02"}
    ,{id:'area03', coords : [110,477,326,617],   cutImage: "cut_P03"}
    ,{id:'area04', coords : [299,383,410,456],   cutImage: "cut_P04"}
    ,{id:'area05', coords : [447,117,768,428],   cutImage: "cut_P05"}
    ,{id:'area06', coords : [928,24,1332,162],   cutImage: "cut_P06"}
    ,{id:'area07', coords : [872,279,1065,403],  cutImage: "cut_P07"}
    ,{id:'area08', coords : [1079,180,1460,297], cutImage: "cut_P08"}
    ,{id:'area09', coords : [1131,306,1286,479], cutImage: "cut_P09"}
    ,{id:'area10', coords : [351,480,1656,718],  cutImage: "cut_P09"}
];

var title;
var scaleFactor;
var flagZoom = false;
var rate;
var currentURL = $(location).attr('pathname');
var imgPath = currentURL.substring(currentURL.lastIndexOf("/"));
imgPath = "../img/" + imgPath.substring(0, imgPath.lastIndexOf("."));

var Agent = navigator.userAgent;
var is_safari = Agent.indexOf("Safari") > -1;
var is_iPad = Agent.match(/iPad/i) != null;

var baseImg = "#img";

$(document).ready(function() {
    initViewer.call(this);
});
