/**
 * Values used for the graph.
 * @type {{GRAPH_TITLE: string, IMAGE_ROOT_PATH: string, CANVAS_WIDTH: number, CANVAS_HEIGHT: number}}
 */
var GRAPH_CONSTANTS = {
    "GRAPH_TITLE" : ''
    , "IMAGE_ROOT_PATH" : 'img/'
    , "CANVAS_WIDTH" : 535
    , "CANVAS_HEIGHT" : 430
}

/**
 * Fabric Group used for the containing the Img object.
 * @type {fabric.Group}
 */
var READY_GRAPH_GROUP = new fabric.Group([], {
    left: 0,
    top: 0
});

var PLAY_GRAPH_GROUP = new fabric.Group([], {
    left: 0,
    top: 0
});


/**
 * Change visible value of the group.
 * @param canvas  The place of the graph image.
 * @param idx     index of the graph content
 * @param subIdx  index of img array of the graph content
 * @param count button click count
 */
function drawGraph(canvas, idx, subIdx, count) {
    var param = JSON.parse(JSON.stringify(CONTENT_INFO[idx]));
    canvas.remove(PLAY_GRAPH_GROUP);
    if(!SLIDE_FLAG) {
        var readyItem = READY_GRAPH_GROUP.item(param.sIdx + subIdx);
        var contains = PLAY_GRAPH_GROUP.contains(readyItem);
        if(contains) {
            PLAY_GRAPH_GROUP.remove(readyItem);
        } else {
            PLAY_GRAPH_GROUP.add(readyItem);
        }
    } else {
        var index = parseInt(count % (param.size));
        for(var i=param.eIdx ; i>=param.sIdx; i--) {
            var item = READY_GRAPH_GROUP.item(i);
            PLAY_GRAPH_GROUP.remove(item);
        }
        //
        // CONTENT_INFO has array of images.
        // If the image value is 'EMPTY' then change the visible value of the group.
        //
        if(param.img[index] != 'EMPTY') {
            for(var i=param.sIdx; i<=(param.sIdx + index); i++) {
                var item = READY_GRAPH_GROUP.item(i);
                PLAY_GRAPH_GROUP.add(item);
            }
        }
    }
    canvas.add(PLAY_GRAPH_GROUP);
    canvas.renderAll();
}

function clearPlayGraphGroup(canvas) {
    PLAY_GRAPH_GROUP.remove();
    canvas.remove(PLAY_GRAPH_GROUP);
    PLAY_GRAPH_GROUP = new fabric.Group([], {
        left: 0,
        top: 0
    });
}


/**
 * Bind the button event.
 * If button has a '.mouseover' class then the button image will be changed  when  mouseover event is fired.
 */
function initButtons() {
//    $('.mouseover').each(function() {
//
//        $(this).mouseover(function() {
//            var mouseOverImage = getMouseoverImageName(this.src);
//            this.src = mouseOverImage;
//        });
//        $(this).mouseout(function() {
//            var originalImage = getOriginalImageName(this.src);
//            this.src = originalImage;
//        });
//    });

    $('#btnTitleOn').click(function() {
        this.src = getToggleImageName(this.src);
        var titleTag = $('#titleArea');
        if(titleTag.text().length > 1) {
            GRAPH_CONSTANTS.GRAPH_TITLE = titleTag.text();
            titleTag.text('');
        } else {
            titleTag.text(GRAPH_CONSTANTS.GRAPH_TITLE);
        }
    });
}


function getImageScaleSize (originalWidth, originalHeight, canvasWidth, canvasHeight) {
    var scaledWidth;
    var scaledHeight;
    var scaledX;
    var scaledY;

    if(originalWidth < canvasWidth) {              // 이미지 가로 길이가 캔버스의 가로보다 작은 경우
//        console.log("originalWidth = " + originalWidth + "/ canvasWidth = " +canvasWidth);
//        console.log(originalWidth + " / " + originalHeight + " / " + canvasWidth + " / " + canvasHeight);
        if(originalHeight > canvasHeight) {        // 이미지 세로 길이가 캔버스 세로 길이보다 크면
            scaledHeight = canvasHeight;              // 이미지 세로의 길이를 캔버스 세로 길이에 맞춘다.
            scaledWidth = Math.ceil(originalWidth * ( canvasHeight / originalHeight)); //세로가 줄어든 비율로 가로를 줄인다.
        } else {                             // 이미지 세로 길이가 캔버스의 세로 길이보다 작은 경우 원본 비율을 그대로 유지한다.
            scaledHeight = originalHeight;  //
            scaledWidth = originalWidth;
        }
    } else {                                                // 이미지 가로 길이가 캔버스 가로 길이보다 큰 경우
        if((originalHeight / originalWidth) > (canvasHeight / canvasWidth) ) { // 캔버스의 가로,세로 비율이 이미지의 가로 세로 비율보다 작은 경우
            scaledHeight = canvasHeight;
            scaledWidth = Math.ceil(originalWidth * (canvasHeight / originalHeight));
        } else {
            scaledWidth = canvasWidth;
            scaledHeight = Math.ceil(originalHeight * (canvasWidth / originalWidth));
        }
    }

    scaledX = scaledWidth / originalWidth;
    scaledY = scaledHeight / originalHeight;

//    console.log(scaledHeight + " / " + canvasHeight + " / " + scaledX);

    return {scaledWidth: scaledWidth, scaledHeight: scaledHeight, scaledX: scaledX, scaledY: scaledY};
}

