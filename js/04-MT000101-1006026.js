var CONTENTS_INFO = [
     {btn: "none"                    ,  imgId: '01_00'}
    ,{btn: "BTN_BOTTOM_FIRST_SUB"    ,  imgId: '01_01'}
    ,{btn: "BTN_BOTTOM_SECOND_SUB"   ,  imgId: '01_02'}
    ,{btn: "BTN_BOTTOM_THIRD_SUB"    ,  imgId: '01_03'}
    ,{btn: "BTN_BOTTOM_FORTH_SUB"    ,  imgId: '01_04'}
    ,{btn: "BTN_BOTTOM_FIFTH_SUB"    ,  imgId: '01_05'}
    ,{btn: "BTN_BOTTOM_SIXTH_SUB"    ,  imgId: '01_06'}
    ,{btn: "BTN_BOTTOM_SEVENTH_SUB"  ,  imgId: '01_07'}
    ,{btn: "BTN_BOTTOM_EIGHTH_SUB"   ,  imgId: '01_08'}
    ,{btn: "BTN_BOTTOM_NINTH_SUB"    ,  imgId: '01_09'}
    ,{btn: "BTN_BOTTOM_TENTH_SUB"    ,  imgId: '01_10'}
    ,{btn: "BTN_BOTTOM_ELEVENTH_SUB" ,  imgId: '01_11'}
    ,{btn: "none"                    ,  imgId: '02_00'}
    ,{btn: "BTN_BOTTOM_FIRST_SUB2"   ,  imgId: '02_01'}
    ,{btn: "BTN_BOTTOM_SECOND_SUB2"  ,  imgId: '02_02'}
    ,{btn: "BTN_BOTTOM_THIRD_SUB2"   ,  imgId: '02_03'}
    ,{btn: "BTN_BOTTOM_FORTH_SUB2"   ,  imgId: '02_04'}
    ,{btn: "BTN_BOTTOM_FIFTH_SUB2"   ,  imgId: '02_05'}
    ,{btn: "BTN_BOTTOM_SIXTH_SUB2"   ,  imgId: '02_06'}
    ,{btn: "BTN_BOTTOM_SEVENTH_SUB2" ,  imgId: '02_07'}
    ,{btn: "BTN_BOTTOM_EIGHTH_SUB2"  ,  imgId: '02_08'}
    ,{btn: "BTN_BOTTOM_NINTH_SUB2"   ,  imgId: '02_09'}
    ,{btn: "BTN_BOTTOM_TENTH_SUB2"   ,  imgId: '02_10'}
    ,{btn: "BTN_BOTTOM_ELEVENTH_SUB2",  imgId: '02_11'}
    ,{btn: 'none'                    ,  imgId: 'img'}
    ,{btn: 'BTN_BOTTOM_ALL'          ,  imgId: 'img3'}
    ,{btn: 'none'                    ,  imgId: 'txtPrescript'}
];

var COORDS_INFO = [
    {id : "area00",   imgId: '01_01',   coords : [161,290,270,400]},
    {id : "area01",   imgId: '01_02',   coords : [290,290,400,400]},
    {id : "area02",   imgId: '01_03',   coords : [421,290,530,400]},
    {id : "area03",   imgId: '01_04',   coords : [550,290,660,400]},
    {id : "area04",   imgId: '01_05',   coords : [680,290,790,400]},
    {id : "area05",   imgId: '01_06',   coords : [810,290,920,400]},
    {id : "area06",   imgId: '01_07',   coords : [940,290,1050,400]},
    {id : "area07",   imgId: '01_08',   coords : [1070,290,1180,400]},
    {id : "area08",   imgId: '01_09',   coords : [1200,290,1310,400]},
    {id : "area09",   imgId: '01_10',   coords : [1330,290,1440,400]},
    {id : "area10",   imgId: '01_11',   coords : [1460,290,1570,400]},

    {id : "area11",   imgId: '02_01',   coords : [161,403,270,1056]},
    {id : "area12",   imgId: '02_02',   coords : [290,403,400,1056]},
    {id : "area13",   imgId: '02_03',   coords : [421,403,530,1056]},
    {id : "area14",   imgId: '02_04',   coords : [550,403,660,1056]},
    {id : "area15",   imgId: '02_05',   coords : [680,403,790,1056]},
    {id : "area16",   imgId: '02_06',   coords : [810,403,920,1056]},
    {id : "area17",   imgId: '02_07',   coords : [940,403,1050,1056]},
    {id : "area18",   imgId: '02_08',   coords : [1070,403,1180,1056]},
    {id : "area19",   imgId: '02_09',   coords : [1200,403,1310,1056]},
    {id : "area20",   imgId: '02_10',   coords : [1330,403,1440,1056]},
    {id : "area21",   imgId: '02_11',   coords : [1460,403,1570,1056]}

];

var GRAPH_INFO = [
    "img","img3",
    "01_00","01_01","01_02","01_03","01_04","01_05","01_06","01_07","01_08","01_09","01_10","01_11",
    "02_00","02_01","02_02","02_03","02_04","02_05","02_06","02_07","02_08","02_09","02_10","02_11",
    "txtPrescript"
];

var scaleFactor;
var flagZoom = false;

var imageTop;
var imageLeft;
var position;
var baseImg = "#img1";


var title;
var pageURL;
var contentID;
var rate;


$(document).ready(function() {
    initBatchViewer.call(this);
    $(".btnUnitView").css('width', '50px');
});


/**
 * Created by Administrator on 2015-04-15.
 */

function initBatchViewer() {

    /**
     * 타이틀 설정
     *
     */
    setTitle.call(this);
    sizeToFit('#container', baseImg, true);

    load.call(this);
    $('.graphBaseImages').hide();
    load.call(this);
    $('.graphBaseImages').show();

    $(window).resize(function () {
        sizeToFit('#container', baseImg, true);
        setTimeout( function () {
            load.call(this);
        }, 150);
    });

    $(".graphImage").hide();
    $("#img").show();
    //$("#img1").hide();
    $("#img3").hide();

    $("#mainTable").on("dragstart"  , function(event){return false;});	// 드래그 방지
    $(window.document).on("selectstart", function(event){return false;});	// 더블클릭 방지
    $(window.document).on("contextmenu", function(event){return false;});	// 우클릭 방지

    $("#BTN_BOTTOM_ALL").click(function () {
        toggleEntireView.call(this);
    });

    $("#btnToggleTitle").click(function() {
        toggleTitle.call(this);
    });

    $("#btnLockScreen").click(function() {
        this.src = getToggleImageName(this.src);
        if (this.src.indexOf('toggle') > 0) {
            $("#mask").hide();
        } else if (this.src.indexOf('toggle') == -1) {
            $("#mask").show();
        }
    });

    $("#img").click(function() {
        //alert("img");
        imgClick(event);
    });

    $("#img3").click(function() {
        //alert("img");
        imgClick(event);
        $('#BTN_BOTTOM_ALL')[0].src = getToggleImageName($('#BTN_BOTTOM_ALL')[0].src);
    });

    $(".unitView").click(function() {
        //alert("img");
        imgClick(event);


    });

    $("#showTxtPrescript").click(function() {
        if (this.src.indexOf("toggle") == -1) {
            $("#showTxtPrescript")[0].src = getToggleImageName($("#showTxtPrescript")[0].src);
            $("#txtPrescript").show();
            setTimeout(function() {
                $("#txtPrescript").hide();
                $("#showTxtPrescript")[0].src = getToggleImageName($("#showTxtPrescript")[0].src);
            },1500);
        }
    });

    /**
     * 클릭 이벤트 처리
     * @param e
     */
    function imgClick(e){
        var x = e.offsetX;
        var y = e.offsetY;
        console.log("x / y =   " + x + " / " + y);

        for (var i = 0; i < COORDS_INFO.length; i++){
            if (contains(x, y, COORDS_INFO[i].coords) == true) {

                var img = COORDS_INFO[i].imgId;
                var isDisplay = $("#" + img).css("display");

                console.log("img =========" + img);

                if (isDisplay == "none") {
                    $("#" + img).show();
                    $('#img3').hide();
                    flagZoom = true;
                } else {
                    $("#" + img).hide();
                    flagZoom = false;
                }
                break;
            }
        }
    }

    /**
     * 클릭 지점이 확대 영역에 포함되는 지 여부 판별
     * @param x
     * @param y
     * @param rect
     * @returns {boolean}
     */
    function contains(x, y, rect){
        var x1 = rect[0] * rate;
        var y1 = rect[1] * rate;
        var x2 = rect[2] * rate;
        var y2 = rect[3] * rate;

        if (x < x1 || x > x2)
            return false;
        if (y < y1 || y > y2)
            return false;

        //console.log("clicked here : "  + x1 + " / " + y1 + " / " + x2 + " / " + y2);
        return true;
    }
} // initBatchViewer

var clickCount = 0;
var currentURL = $(location).attr('pathname');
currentURL = currentURL.substring(currentURL.lastIndexOf("/"));

/**
 * [jgkim] 2015.04.15
 * 이미지 조정 시작
 * 아이패드에서 딜레이 필요(확인 중)
 * @param baseImg
 * @param flagZoom
 */
function load() {

    for (var i = 0; i < GRAPH_INFO.length; i++) {
        var graphImg = "#" + GRAPH_INFO[i];

        var baseWidth = $(baseImg).css('width');
        var baseHeight = $(baseImg).css('height');

        $(graphImg).css('width', baseWidth);
        $(graphImg).css('height', baseHeight);

        var position = $(baseImg).offset();

        var imgLeft = position.left;
        var imgTop = position.top;

        $(graphImg).css({
            position: "absolute",
            left: imgLeft,
            top: imgTop
        });

        $('.graphBaseImages').css({
            position: "absolute",
            left: imgLeft,
            top: imgTop
        });
    }

    $(".bottomBtn").css('min-width', "80px");
    $(".bottomBtn").css('width', $(window).width() * 0.06);
    $("#BTN_BOTTOM_MODE").css('width', $(window).width() * 0.12);
    $("#BTN_BOTTOM_MODE").css('width', $(window).width() * 0.12);
    $(".titleOn").css('width', $(window).width() * 0.09);
}

/**
 * [jgkim] 2015.04.15
 * 이미지 리사이즈 주 로직.
 * @param source
 * @param dest
 * @param isSet
 */
function sizeToFit(source, dest, isSet) {

    var marginH = 50;   // margin height

    var tWidth = $(source).width();             // target width
    var tHeight = $(window).height();           // target height

    var bottomHeight = $("#bottom").height();
    tHeight -= bottomHeight + marginH;

    var sWidth = $(dest)[0].naturalWidth;
    var sHeight = $(dest)[0].naturalHeight;

    var rateWidth = tWidth / sWidth;
    var rateHeight = tHeight / sHeight;

    rate = rateWidth < rateHeight ? rateWidth : rateHeight;
    var width = sWidth * rate;
    var height = sHeight * rate;

    $(dest).attr("width", width + "px");
    $(dest).attr("height", height + "px");

    if (isSet == true)
        scaleFactor = rate;
}


/**
 * [jgkim] 2015.04.17
 * 타이틀 설정
 *
 *
 */
function setTitle() {
    pageURL = this.location.href.substring(this.location.href.lastIndexOf('/') + 1);
    contentID = pageURL.substring(0, pageURL.lastIndexOf('.'));

    for (var i = 0; i < fullContentsArray.enabledContents.length; i++) {
        if (contentID === fullContentsArray.enabledContents[i].cd) {
            title = fullContentsArray.enabledContents[i].tit;
        }
    }
    $("title").html(title);
    $("#titleArea").html(title);

    $("#mask").css({
        width: $(window).width() - 150,
        height: $(window).height(),
        position: "absolute",
        top: 0,
        left: "150px",
        opacity: 0,
        backgroundColor: "black",
        'z-index': 999,
        display: 'none'
    });
}

/**
 * [jgkim] 2015.04.21
 * 타이틀 토글
 *
 */
function toggleTitle() {
    this.src = getToggleImageName(this.src);
    var titleTag = $('#titleArea');
    if(titleTag.text().length > 1) {
        titleTag.text('');
    } else {
        titleTag.text(title);
    }
}

/**
 * [jgkim] 2015.04.17
 * 전체보기(엔타이어뷰) 동작
 *
 */
function toggleEntireView() {
    if (this.src.indexOf('toggle') == -1) {
        $('#img3').hide();
    } else {
        initializeGraphImgs();
        $('#img3').show();
    }
    $(this)[0].src = getToggleImageName($(this)[0].src);
    clickCount = 0;
}

/**
 * [jgkim] 2015.04.17
 * 보기 모드 토글 동작
 *
 */
function toggleBtnMode() {
    initializeGraphImgs();
    $('#img3').hide();
    $(this)[0].src = getToggleImageName($(this)[0].src);
    $("#BTN_BOTTOM_ALL")[0].src = getToggleOffImageName($("#BTN_BOTTOM_ALL")[0].src);
}

function toggleOFFTargetButton(target) {
    $(target)[0].src = getToggleOffImageName($(target)[0].src);
}

function initializeGraphImgs() {
    $('.unitView').hide();
    $('.bundleView').hide();
    $('.btnUnitView').each(function() {
        $(this)[0].src = getToggleOnImageName($(this)[0].src);
    });
    if($('#BTN_BOTTOM_MODE').length > 0) {
        CONTENTS_INFO_2 = JSON.parse(JSON.stringify(oCONTENTS_INFO_2));
    }
    clickCount = 0;
}



