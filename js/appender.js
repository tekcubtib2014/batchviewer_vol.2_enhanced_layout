///////////////////////////////////////////////////////////////
//////                Appender(Abstract)                   ////
///////////////////////////////////////////////////////////////
function Appender() {}


///////////////////////////////////////////////////////////////
//////                    ConsoleAppender                  ////
///////////////////////////////////////////////////////////////
/**
 * Construct Console Appender.
 * This appender will write log to javascript console.
 * @param name Appender name
 */
function ConsoleAppender(name) {
    this.TYPE = Enum.AppenderType.CONSOLE;
    this.name = name;
    Appender.call(this);
}
// inherit Appender
ConsoleAppender.prototype.constructor = ConsoleAppender;
ConsoleAppender.prototype = new Appender();

/**
 * Write log to console. If appender can't write log on console then raise event onFailEvent.
 * @param msg   {string} log parameter.
 * @param fail  {function} callback method when exception is occurred.
 */
ConsoleAppender.prototype.log = function(msg, onFailEvent) {
    try {
        console.log(msg.constructor == Object ? JSON.stringify(msg) : msg);
    } catch( err ) {
        console.error(err.message);
        if( !(onFailEvent === undefined)) {
            onFailEvent(msg);
        }
    }
}

ConsoleAppender.prototype.write = function(msg, onFailEvent) {
    var result = false;
    try {
        console.log(msg.constructor == Object ? JSON.stringify(msg) : msg);
        result = true;
    } catch( err ) {
        console.error(err.message);
    }
    return result;
}

///////////////////////////////////////////////////////////////
//////                    FileAppender                     ////
///////////////////////////////////////////////////////////////
/**
 * Construct Console Appender.
 * This appender will write log to local file.
 * @param name Appender name
 */
function FileAppender(fileAppenderName) {
    var TYPE = Enum.AppenderType.FILE;
    this.fileAppenderName = fileAppenderName;
    var writer = undefined;
    Appender.call(this);

    this.getAppenderType = function() {
        return TYPE;
    }
    this.getAppenderName = function() {
        return fileAppenderName;
    }

    var onFail = function(evt) {
        console.log("target=" + evt.target);
        console.log("evt= "  + evt);
        console.log("code= " + evt.code);
    }
    var onGetFileWriterSuccess = function(writer) {
        writer.seek(writer.length);
        setWriter(writer);
        writer.onwriteend = function() {
            writer.abort();
        }
    }
    var onGetFileEntrySuccess = function(fileEntry) {
        fileEntry.createWriter(onGetFileWriterSuccess, onFail)
    }

    var onGetFileSystemSuccess = function(fileSystem) {
        fileSystem.root.getFile(Enum.Const.LOG_FILE_NAME, {create: true, exclusive: false}, onGetFileEntrySuccess, onFail);
    }

    var onDeviceReady = function() {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onGetFileSystemSuccess, onFail);
    }

    document.addEventListener("deviceready", onDeviceReady, false);

    this.getWriter = function() {
        return writer;
    }

    var setWriter = function(w) {
        writer = w;
    }
}

// inherit Appender
FileAppender.prototype.constructor = FileAppender;
FileAppender.prototype = new Appender();

FileAppender.prototype.write = function ( args, onFailEvent) {
    var result = false;
    try {
        if(!(this.getWriter() === undefined)) {
            var msg = (args.constructor === String ? args : JSON.stringify( args ));
            this.getWriter().write( msg );
            result = true;
        }
    } catch( err ) {
        console.error( err.message );
        onFailEvent(args);
    }
    return result;
}



///////////////////////////////////////////////////////////////
//////                    HttpPostAppender                 ////
///////////////////////////////////////////////////////////////
/**
 * Appender for HTTP POST
 * @param name Appender name
 */
function HttpPostAppender(httpAppenderName) {
    this.TYPE = Enum.AppenderType.HTTP_POST;
    var httpAppenderName = httpAppenderName;
    var targetUrl;
    var fileWriter;
    Appender.call(this);

    this.getAppenderName = function() {
        return httpAppenderName;
    }

    this.getTagetUrl = function() {
        return targetUrl;
    }

    this.setTargetUrl = function(url) {
        targetUrl = url;
    }

    var setFileWriter = function(writer) {
        fileWriter = writer;
    }

    this.getFileWriter = function() {
        return fileWriter;
    }

    //
    // TODO 백업된 데이터를 서버로 전송한 후 삭제한다.
    //

    var onFail = function(evt) {
        console.log("evt= "  + evt);
        console.log("code= " + evt.code);
    }

    function onGetFileSuccess(file) {
        var fileReader = new FileReader();
        fileReader.onloadend = function(evt) {
            console.log(evt.target.result);
            $.ajax({
                type: "POST"
                , dataType: "text"
                , contentType: "text/xml"
                , url: targetUrl
                , data: evt.target.result
                , statusCode: {
                    200: function () {
                        console.log("code [200] // success");
                        console.log("===================== BACKUP DELETE =======================");
                        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
                            fileSystem.root.getFile(Enum.Const.LOG_FILE_NAME, {create: false, exclusive: false}, function(fileEntry) {
                                if( !(fileEntry === undefined) ) {
                                    fileEntry.remove(function() {
                                        console.log("Backupfile is deleted");
                                    });
                                }
                            });
                        });

                    }, 401: function() {console.log("code [401] // ERROR");  }
                    , 500: function() {console.log("code [500] // ERROR"); }
                }, error: function (request, status, error) {
                    console.log("error: " + error + " code=" + request.status);
                }
            });
        }
        fileReader.readAsText(file)
    }
    var onGetFileEntrySuccess = function(fileEntry) {
        fileEntry.file(onGetFileSuccess, onFail);
    }

    var onGetFileSystemSuccess = function(fileSystem) {
        fileSystem.root.getFile(Enum.Const.LOG_FILE_NAME, {create: false, exclusive: false}, onGetFileEntrySuccess, onFail);
    }

    var onDeviceReady = function() {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onGetFileSystemSuccess, onFail);
    }
    if(Enum.Const.LOG_FILE_NAME != undefined) {
        document.addEventListener("deviceready", onDeviceReady, false);
    }
}


// inherit appender.
HttpPostAppender.prototype.constructor = HttpPostAppender;
HttpPostAppender.prototype = new Appender();

HttpPostAppender.prototype.logBackup = function(data) {
    console.log("===================== LOGBACKUP =======================");
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
        fileSystem.root.getFile(Enum.Const.LOG_FILE_NAME, {create: true, exclusive: false}, function(fileEntry) {
            fileEntry.createWriter(function(writer) {
                writer.write(data);
                writer.abort();
            });
        })
    });
}

HttpPostAppender.prototype.write = function(data, onFailEvent) {
    try {
        $.ajax({
            type: "POST"
            , dataType: "text"
            , contentType: "text/xml"
            , url: this.getTagetUrl()
            , data: data
            , statusCode: {
                200: function () {console.log("code [200] // success");}
                , 401: function () {
                    onFailEvent(data);
                }
                , 500: function() {
                    console.log("code [500] // fail");
                    onFailEvent(data);
                }
            }, error: function(request, status, error) {
                console.log("code: " + request.status + ", message="+request.responseText + ", error: "+ error);
                onFailEvent(data);
            }
        });
    } catch (e) {
        console.error(e);
    }
}