function openURL(url) {

    var customUrlIndex = url.lastIndexOf('/');
    if(customUrlIndex == -1) {
        return;
    }
    function checkCert() {

        var param = url.substring(url.lastIndexOf("/") + 1, url.length);
        for (var index = 0, len = contentsArray.enabledContents.length; index < len; index++) {

            //
            // contents.js 의 데이터와 비교하여 호출된 콘텐츠 아이디가 있을 경우
            // 해당 페이지로 이동한다.
            //
            if (contentsArray.enabledContents[index].cd == param) {
                customUrlScheme(url);
                return false;
            }
        }
        location.replace("index.html");
    }

    document.addEventListener("deviceready", function() {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
            fileSystem.root.getFile(Enum.Const.LICENSE_FLAG, {create: false, exclusive: false}, function() {
                contentsArray.enabledContents = fullContentsArray.enabledContents;
                checkCert();
            }, function() {
                checkCert();
            });
        });
    })


}


/**
 * NAVI 앱에서 특정 컨텐츠 아이디를 파라미터로 넘기는 경우,
 * 해당 컨텐츠를 직접 호출한다.
 * */
function customUrlScheme(url) {

    var customUrlIndex = url.lastIndexOf('/');
    if(customUrlIndex == -1) {
        return;
    }
    var param = url.substring(url.lastIndexOf("/") + 1, url.length);
    if(param == null || param == "") {
        return;
    }
    for(var i in contentsArray.enabledContents) {
        var index = contentsArray.enabledContents[i];

        if(param == index.cd) {
            location.href = (param + ".html");
            return false;
        }
    }
}

function openUrlInContents(url) {
    var customUrlIndex = url.lastIndexOf('/');
    if(customUrlIndex == -1) {
        return;
    }
    var param = url.substring(url.lastIndexOf("/") + 1, url.length);
    if(param == null || param == "") {
        return;
    }

    if(window.localStorage.getItem('cert') == 'true') {
        for(var i in fullContentsArray.enabledContents) {
            var index = fullContentsArray.enabledContents[i];
            if(param == index.cd) {
                location.href = (param + ".html");
                return false;
            }else {
            }
        }
    }else {
        for(var i in contentsArray.enabledContents) {
            var index = contentsArray.enabledContents[i];
            if(param == index.cd) {
                location.href = (param + ".html");
                return false;
            }else {
            }
        }
    }
}