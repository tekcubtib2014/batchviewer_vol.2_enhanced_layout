
var CONTENTS_INFO = [
     {btn: 'BTN_BOTTOM_MIDDLE_FIRST',       imgId: '01_01'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SECOND',      imgId: '01_02'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_THIRD',       imgId: '01_03'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_FORTH',       imgId: '02_01'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_FIFTH',       imgId: '02_02'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SIXTH',       imgId: '02_03'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SEVENTH',     imgId: '02_04'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_EIGHTH',      imgId: '02_05'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_NINTH',       imgId: '02_06'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_TENTH',       imgId: '02_07'}

    ,{btn: 'none',                          imgId: 'img'}
    ,{btn: 'BTN_BOTTOM_ALL',                imgId: 'img3'}
];

var GRAPH_INFO =
    [
        'img', 'img3',
        "01_01","01_02","01_03",
        "02_01","02_02","02_03","02_04","02_05","02_06","02_07"
    ];

var scaleFactor;
var flagZoom = false;

var imageTop;
var imageLeft;
var position;
var baseImg = "#img1";


var title;
var pageURL;
var contentID;


$(document).ready(function() {
    initBatchViewer.call(this);
});