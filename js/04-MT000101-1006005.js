

//
// 레이아웃 수정 전 설정했던 좌표정보. [jgkim] 2015.04.09
//
var COORDS_INFO = [
     {id:'area01',   coords:[71,194,511,404],       cutImage: "cut_P01"}
    ,{id:'area02',   coords:[19,438,262,620],       cutImage: "cut_P02"}
    ,{id:'area03',   coords:[307,454,563,607],      cutImage: "cut_P03"}
    ,{id:'area04',   coords:[431,617,642,747],      cutImage: "cut_P04"}
    ,{id:'area05',   coords:[590,91,799,454],       cutImage: "cut_P05"}
    ,{id:'area06',   coords:[911,20,1275,539],      cutImage: "cut_P06"}
    ,{id:'area07',   coords:[887,577,1147,743],     cutImage: "cut_P07"}
];

var title;
var scaleFactor;
var flagZoom = false;
var rate;
var currentURL = $(location).attr('pathname');
var imgPath = currentURL.substring(currentURL.lastIndexOf("/"));
imgPath = "../img/" + imgPath.substring(0, imgPath.lastIndexOf("."));

var Agent = navigator.userAgent;
var is_safari = Agent.indexOf("Safari") > -1;
var is_iPad = Agent.match(/iPad/i) != null;

var baseImg = "#img1";

$(document).ready(function() {
    initViewer.call(this);
});


