/**
 * Created by Administrator on 2015-04-24.
 */


function Person(name, blog) {
    this.name = name;
    this.blog = blog;
}

Person.prototype = {
    getName : function() {
        return this.name;
    },
    getblog : function() {
        return this.blog;
    }
};

var unikys = new Person("unikys", "unikys.com");
var stranger = new Person("stranger", "google.com");

console.log(unikys.getName());
console.log(unikys.getblog());
console.log(stranger.getName());
console.log(stranger.getblog());

Person.prototype.introduce = function() {
    console.log(this.getName + " / " + this.getblog());
};
stranger.introduce();