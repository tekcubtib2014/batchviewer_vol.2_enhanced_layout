
var CONTENTS_INFO = [
     {btn: 'BTN_BOTTOM_MIDDLE_FIRST',   imgId: '01_00'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SECOND',  imgId: '02_00'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_THIRD',   imgId: '03_00'}

    ,{btn: 'none',                      imgId: 'img'}
    ,{btn: 'BTN_BOTTOM_ALL',            imgId: 'img3'}
];

var CONTENTS_INFO_2 = [
     {btn: 'BTN_BOTTOM_MIDDLE_FIRST',   images:["01_01","01_02","01_03","01_04","01_05","01_06","01_07","01_08","EMPTY"]}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SECOND',  images:["02_01","02_02","02_03","02_04","02_05","02_06","02_07","02_08","EMPTY"]}
    ,{btn: 'BTN_BOTTOM_MIDDLE_THIRD',   images:["03_01","03_02","03_03","03_04","EMPTY"]}
];

var GRAPH_INFO =
    [
        'img', 'img3',
        "01_00","02_00","03_00",
        "01_01","01_02","01_03","01_04","01_05","01_06","01_07","01_08",
        "02_01","02_02","02_03","02_04","02_05","02_06","02_07","02_08",
        "03_01","03_02","03_03","03_04"
    ];

var scaleFactor;
var flagZoom = false;

var imageTop;
var imageLeft;
var position;
var baseImg = "#img1";

var title;
var pageURL;
var contentID;

var oCONTENTS_INFO_2 = JSON.parse(JSON.stringify(CONTENTS_INFO_2));


$(document).ready(function() {
    initBatchViewer.call(this);
});