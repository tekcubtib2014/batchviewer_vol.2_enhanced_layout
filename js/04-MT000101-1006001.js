
var COORDS_INFO = [
     {id:'area01',      coords : [54,126,380,362],         cutImage: "cut_P01"}
    ,{id:'area02',      coords : [67,389,424,664],         cutImage: "cut_P02"}
    ,{id:'area03',      coords : [497,154,680,260],        cutImage: "cut_P03"}
    ,{id:'area04',      coords : [576,287,729,354],        cutImage: "cut_P04"}
    ,{id:'area05',      coords : [437,366,653,533],        cutImage: "cut_P05"}
    ,{id:'area06',      coords : [736,142,886,309],        cutImage: "cut_P06"}
    ,{id:'area07',      coords : [682,412,910,588],        cutImage: "cut_P07"}
    ,{id:'area08',      coords : [918,246,1091,402],       cutImage: "cut_P08"}
    ,{id:'area09',      coords : [1145,56,1333,327],       cutImage: "cut_P09"}
    ,{id:'area10',      coords : [1367,176,1587,307],      cutImage: "cut_P10"}
    ,{id:'area11',      coords : [1260,342,1538,497],      cutImage: "cut_P11"}
    ,{id:'area12',      coords : [936,428,1241,525],       cutImage: "cut_P12"}
    ,{id:'area13',      coords : [926,528,1648,713],       cutImage: "cut_P13"}
];

var title;
var scaleFactor;
var flagZoom = false;
var rate;
var currentURL = $(location).attr('pathname');
var imgPath = currentURL.substring(currentURL.lastIndexOf("/"));
imgPath = "../img/" + imgPath.substring(0, imgPath.lastIndexOf("."));

var Agent = navigator.userAgent;
var is_safari = Agent.indexOf("Safari") > -1;
var is_iPad = Agent.match(/iPad/i) != null;

var baseImg = "#img";

$(document).ready(function() {
    initViewer.call(this);
});
