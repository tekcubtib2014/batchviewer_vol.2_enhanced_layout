/**
 * Get formatted string
 * @return {string} formatted string yyyyMMddHHmmss
 */
function getYYYYMMDDHHmmss() {
    var now = new Date();
    var yyyy    = ("0000"+ now.getFullYear()).slice(-4);
    var MM      = ("00"  + (now.getMonth() + 1)).slice(-2);
    var dd      = ("00"  + now.getDate()).slice(-2);
    var hh      = ("00"  + now.getHours()).slice(-2);
    var mm      = ("00"  + now.getMinutes()).slice(-2);
    var ss      = ("00"  + now.getSeconds()).slice(-2);
    return yyyy+MM+dd+hh+mm+ss;
}

/**
 * Get formatted string
 * @return {string} formatted string yyyy-MM-dd HH:mm:ss
 */
function getLogFormattedDate() {
    var now = new Date();
    var yyyy    = ("0000"+ now.getFullYear()).slice(-4);
    var MM      = ("00"  + (now.getMonth() + 1)).slice(-2);
    var dd      = ("00"  + now.getDate()).slice(-2);
    var hh      = ("00"  + now.getHours()).slice(-2);
    var mm      = ("00"  + now.getMinutes()).slice(-2);
    var ss      = ("00"  + now.getSeconds()).slice(-2);
    var mil     = ("000" + now.getMilliseconds()).slice(-3);
    return yyyy+"-"+MM+"-"+dd+" "+hh+":"+mm+":"+ss + "."+mil;
}