
var COORDS_INFO = [
     {id:'area01', coords : [17,152,174,218],    cutImage: "cut_P01"}
    ,{id:'area02', coords : [38,354,375,643],    cutImage: "cut_P02"}
    ,{id:'area03', coords : [281,217,514,286],   cutImage: "cut_P03"}
    ,{id:'area04', coords : [28,221,181,291],   cutImage: "cut_P04"}
    ,{id:'area05', coords : [520,174,654,234],   cutImage: "cut_P05"}
    ,{id:'area06', coords : [687,65,947,181],    cutImage: "cut_P06"}
    ,{id:'area07', coords : [753,233,856,311],   cutImage: "cut_P07"}
    ,{id:'area08', coords : [867,232,956,371],   cutImage: "cut_P08"}
    ,{id:'area09', coords : [732,392,1111,639],  cutImage: "cut_P09"}
    ,{id:'area10', coords : [966,259,1127,363],  cutImage: "cut_P10"}
    ,{id:'area11', coords : [1020,53,1272,226],  cutImage: "cut_P11"}
    ,{id:'area12', coords : [1147,242,1253,384], cutImage: "cut_P12"}
    ,{id:'area13', coords : [1275,250,1497,395], cutImage: "cut_P13"}
    ,{id:'area14', coords : [1256,461,1588,673], cutImage: "cut_P14"}
    ,{id:'area15', coords : [1507,211,1670,419],  cutImage: "cut_P15"}
    ,{id:'area16', coords : [1291,42,1670,208], cutImage: "cut_P15"}
    ,{id:'area17', coords : [409,288,654,371], cutImage: "cut_P17"}
];

var title;
var scaleFactor;
var flagZoom = false;
var rate;
var currentURL = $(location).attr('pathname');
var imgPath = currentURL.substring(currentURL.lastIndexOf("/"));
imgPath = "../img/" + imgPath.substring(0, imgPath.lastIndexOf("."));

var Agent = navigator.userAgent;
var is_safari = Agent.indexOf("Safari") > -1;
var is_iPad = Agent.match(/iPad/i) != null;

var baseImg = "#img";

$(document).ready(function() {
    initViewer.call(this);
});

