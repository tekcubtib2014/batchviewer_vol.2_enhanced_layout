

//
// 레이아웃 수정 전 설정했던 좌표정보. [jgkim] 2015.04.09
//
//var COORDS_INFO = [
//     {id:'area01', coords : [22,64,417,267],       cutImage: "cut_P01"}
//    ,{id:'area02', coords : [11,454,322,701],      cutImage: "cut_P02"}
//    ,{id:'area03', coords : [386,485,533,632],     cutImage: "cut_P03"}
//    ,{id:'area04', coords : [495,240,1057,474],    cutImage: "cut_P04"}
//    ,{id:'area05', coords : [566,490,909,630],     cutImage: "cut_P05"}
//    ,{id:'area06', coords : [854,33,1187,216],     cutImage: "cut_P06"}
//    ,{id:'area07', coords : [948,495,1151,776],    cutImage: "cut_P07"}
//    ,{id:'area08', coords : [1255,136,1652,503],   cutImage: "cut_P08"}
//    ,{id:'area09', coords : [1187,545,1652,899],   cutImage: "cut_P09"}
//    ,{id:'area10', coords : [414,667,927,1049],    cutImage: "cut_P10"}
//    ,{id:'area11', coords : [21,793,423,1046],  cutImage: "cut_P10"}
//];

var COORDS_INFO = [
     {id:'area01', coords : [26,57,422,265],       cutImage: "cut_P01"}
    ,{id:'area02', coords : [6,448,314,696],      cutImage: "cut_P02"}
    ,{id:'area03', coords : [384,473,534,636],     cutImage: "cut_P03"}
    ,{id:'area04', coords : [492,240,1053,470],    cutImage: "cut_P04"}
    ,{id:'area05', coords : [567,478,908,633],     cutImage: "cut_P05"}
    ,{id:'area06', coords : [842,42,1176,225],     cutImage: "cut_P06"}
    ,{id:'area07', coords : [948,495,1138,759],    cutImage: "cut_P07"}
    ,{id:'area08', coords : [1253,149,1649,499],   cutImage: "cut_P08"}
    ,{id:'area09', coords : [1178,547,1647,948],   cutImage: "cut_P09"}
    ,{id:'area10', coords : [414,667,870,795],    cutImage: "cut_P10"}
    ,{id:'area11', coords : [21,795,870,1040],  cutImage: "cut_P10"}
    ,{id:'area12', coords : [895,888,1088,1005],  cutImage: "cut_P12"}
];



var title;
var scaleFactor;
var flagZoom = false;
var rate;
var currentURL = $(location).attr('pathname');
var imgPath = currentURL.substring(currentURL.lastIndexOf("/"));
imgPath = "../img/" + imgPath.substring(0, imgPath.lastIndexOf("."));

var Agent = navigator.userAgent;
var is_safari = Agent.indexOf("Safari") > -1;
var is_iPad = Agent.match(/iPad/i) != null;

var baseImg = "#img";

$(document).ready(function() {
   initViewer.call(this);
});


