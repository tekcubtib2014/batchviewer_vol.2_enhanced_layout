
var CONTENTS_INFO = [
     {btn: 'BTN_BOTTOM_MIDDLE_FIRST',   imgId: '01_01'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SECOND',  imgId: '01_02'}

    ,{btn: 'BTN_BOTTOM_MIDDLE_THIRD',   imgId: '01_03'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_FORTH',   imgId: '01_04'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_FIFTH',   imgId: '01_05'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SIXTH',   imgId: '01_06'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_SEVENTH', imgId: '01_07'}
    ,{btn: 'BTN_BOTTOM_MIDDLE_EIGHTH',  imgId: '01_08'}

    ,{btn: 'none',                      imgId: 'img'}
    ,{btn: 'BTN_BOTTOM_ALL',            imgId: 'img3'}
];

var GRAPH_INFO =
    [
        'img', 'img3',
        "01_01","01_02","01_03","01_04","01_05","01_06","01_07","01_08"
    ];

var scaleFactor;
var flagZoom = false;

var imageTop;
var imageLeft;
var position;
var baseImg = "#img1";


var title;
var pageURL;
var contentID;


$(document).ready(function() {
    initBatchViewer.call(this);
});

