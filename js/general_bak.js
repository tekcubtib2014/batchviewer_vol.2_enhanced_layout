/**
 * Created by Administrator on 2015-04-15.
 */

function initBatchViewer() {

    var Agent = navigator.userAgent;
    var is_safari = Agent.indexOf("Safari") > -1;
    var is_iPad = Agent.match(/iPad/i) != null;

    /**
     * Ÿ��Ʋ ����
     *
     */
    setTitle.call(this);

    $(".graphImage").hide();
    $("#img").hide();
    //$("#img1").show();
    $("#img2").hide();
    $("#img3").hide();

    $("#img").css('border', 'none');

    $("#mainTable").on("dragstart"  , function(event){return false;});	// �巡�� ����
    $(window.document).on("selectstart", function(event){return false;});	// ����Ŭ�� ����
    $(window.document).on("contextmenu", function(event){return false;});	// ��Ŭ�� ����

    setTimeout(function() {
        load(baseImg, flagZoom);
        $(".bottomBtn").css('width', $(window).width() * 0.06);          // ���� ��ư��
        $("#BTN_BOTTOM_MODE").css('width', $(window).width() * 0.12);    // ���? ���? ��ư
    }, 50);

    $('.dialog').hide();

    /**
     * ���̾�α�? �������� ��ư�鿡 ���� ����
     *
     * ��ư�� Ŭ���Ǹ� �ش� ��ư�� �̹����� on / off ����Ѵ�?.
     * �ش� ��ư�� ���ε� �̹����� ǥ���Ѵ�.
     *
     * ��ü����     : ���? �׷��� �̹����� ǥ���Ѵ�.
     * ���? ���?
     * ����
     * ���ֺ�
     *
     */


    /**
     * ��ü���� ��ư ����.
     *
     * ��ü���Ⱑ Ŭ���Ǹ� �ٸ� ���? ��ư�� �ʱ�ȭ �ȴ�.
     * ������, ����, ���� �� ���� ��ü���� ��ư�� �ʱ�ȭ�Ѵ�.
     *
     */
    $("#BTN_BOTTOM_ALL").click(function () {
        toggleEntireView.call(this);
    });

    /**
     * ���? ���? ��ư ����
     *
     * �����? ���ֺ� ���? ���θ� ����Ѵ�?.
     *
     */
    $("#BTN_BOTTOM_MODE").click(function () {
        toggleBtnMode.call(this);
    });

    /**
     * ���ֺ��? ���� ����
     *
     */
    $('.btnUnitView').click(function () {
        toggleBundleOrUnitView.call(this);
    });


    /**
     * ��ư ������ ����
     *
     */
    $(".bottomBtn").css('width', '100px');          // ���� ��ư��
    $("#BTN_BOTTOM_MODE").css('width', '200px');    // ���? ���? ��ư


    function bindClickEventForDialog() {
        $('.btnCloseDialog').click(function () {
            $(this).parents(".dialog").hide();
            if (this.id == "btnCloseDialog") {
                $('#showDialog')[0].src = getToggleImageName($('#showDialog')[0].src);
            } else if (this.id == "btnCloseBundleViewDialog") {
                $('#showDialogBundleView')[0].src = getToggleImageName($('#showDialogBundleView')[0].src);
            } else if (this.id == "btnCloseUnitViewDialog") {
                $('#showDialogUnitView')[0].src = getToggleImageName($('#showDialogUnitView')[0].src);
            }
        });

        $('#showDialog').click(function () {
            if (this.src.indexOf('toggle') > 0) {
                $('#dialog').hide();
            } else if (this.src.indexOf('toggle') == -1) {
                $('#dialog').show();
            }
            $(this)[0].src = getToggleImageName($(this)[0].src);
        });

        $('#showDialogBundleView').click(function () {
            if (this.src.indexOf('toggle') > 0) {
                $('#dialogBundleView').hide();
            } else if (this.src.indexOf('toggle') == -1) {
                $('#dialogBundleView').show();
            }
            $(this)[0].src = getToggleImageName($(this)[0].src);
        });

        $('#showDialogUnitView').click(function () {
            if (this.src.indexOf('toggle') > 0) {
                $('#dialogUnitView').hide();
            } else if (this.src.indexOf('toggle') == -1) {
                $('#dialogUnitView').show();
            }
            $(this)[0].src = getToggleImageName($(this)[0].src);
        });
    }

    bindClickEventForDialog();

    $("#btnToggleTitle").click(function() {
        toggleTitle.call(this);
    });

    $("#btnLockScreen").click(function() {
        this.src = getToggleImageName(this.src);
        if (this.src.indexOf('toggle') > 0) {
            $("#mask").hide();
            $(".hideDialog").each(function() {
                $(this).show();
                $(this).removeClass("hideDialog");
            });

            bindClickEventForDialog();
        } else if (this.src.indexOf('toggle') == -1) {
            $("#mask").show();
            $(".dialog").each(function() {
                if($(this).css("display") != "none") {
                    $(this).addClass("hideDialog");
                    $(this).hide();
                }
            });
            $("#showDialogBundleView").off("click");
            $("#showDialogUnitView").off("click");
            $("#showDialog").off("click");
        }
    });

    /*
     * [2015.05.13] jgkim
     * set the z-index of latest activated window to upper layer.
     * need to refactor. should be excuted by dragging event.
     *
     */
    $("#dialogUnitView").mousedown(function() {
        $("#dialogUnitView").css("z-index", 901);
        $("#dialogBundleView").css("z-index", 900);

    });

    $("#dialogBundleView").mousedown(function() {
        $("#dialogBundleView").css("z-index", 901);
        $("#dialogUnitView").css("z-index", 900);
    });


    if (is_iPad) {
        $(document).bind('touchstart', function (event) {
            $('#dialog').draggable({
                containment: "#container",
                iframeFix: true,
                revert: 'invalid',
                stop: function (event, ui) {
                    $(this).draggable('option', 'revert', 'invalid');
                },
                zIndex: 998
            });
            $("#container").droppable({});
        });
    }
}

var clickCount = 0;
var currentURL = $(location).attr('pathname');
currentURL = currentURL.substring(currentURL.lastIndexOf("/"));

/**
 * [jgkim] 2015.04.15
 * �̹��� ���� ����
 * �����е忡�� ������ �ʿ�(Ȯ�� ��)
 * @param baseImg
 * @param flagZoom
 */
function load(baseImg, flagZoom) {

    sizeToFit("#container", baseImg, true);

    if (flagZoom == true) {
        sizeToFit("#container", baseImg, false);
    }

    sizeToFit2(baseImg);
    $("#img1").show();
    $("#img").show();

    if( $("#img2").length > 0 ) {
        $("#img2").show();
    }
}

/**
 * [jgkim] 2015.04.15
 * �̹��� �������� �� ����.
 * @param source
 * @param dest
 * @param isSet
 */
function sizeToFit(source, dest, isSet) {

    var marginH = 50;   // margin height

    var tWidth = $(source).width();             // target width
    var tHeight = $(window).height();           // target height

    var bottomHeight = $("#bottom").height();
    tHeight -= bottomHeight + marginH;

    var sWidth = $(dest)[0].naturalWidth;
    var sHeight = $(dest)[0].naturalHeight;

    var rateWidth = tWidth / sWidth;
    var rateHeight = tHeight / sHeight;

    var rate = rateWidth < rateHeight ? rateWidth : rateHeight;
    var width = sWidth * rate;
    var height = sHeight * rate;

    $(dest).attr("width", width + "px");
    $(dest).attr("height", height + "px");

    if (isSet == true)
        scaleFactor = rate;
}

/**
 * [jgkim] 2015.04.15
 * �̹��� �������̿� ����ϱ�? ���� �������� ���? ��ǥ�� ����.
 * @param baseImg
 */
function sizeToFit2(baseImg) {
    for (var i = 0; i < CONTENTS_INFO.length; i++) {
        var img = "#" + CONTENTS_INFO[i].imgId;
        //adjustGraphImages(img, baseImg);

        sizeToFit("#container", img, false);

        var position = $("#img1").offset();

        imageLeft = position.left;
        imageTop = position.top;

        console.log(position.left + "   ///////  " + position.top);

        //imageLeft = ( ($('#container').width() - $(baseImg).width()) / 2 ) + 150;
        //imageTop = ($(window).height() - $(baseImg).height()) / 2 + 20;
        //imageTop = 45;

        $(img).css({
            position: "absolute",
            left: imageLeft,
            top: imageTop
        });
    }

    if($('#BTN_BOTTOM_MODE').length > 0) {
        for(var j = 0; j < CONTENTS_INFO_2.length; j++) {
            var unitViewArray = CONTENTS_INFO_2[j].images;
            var removeItem = "EMPTY";
            unitViewArray = jQuery.grep(unitViewArray, function(value) {
                return value != removeItem;
            });
            for(var k = 0; k < unitViewArray.length; k++) {
                var img =  "#" + unitViewArray[k];
                adjustGraphImages(img, baseImg);
            }
        }
    }

    $('#dialogBundleView').css({
        left: "150px",
        top: "70px",
        "z-index" : "900"
    });

    $('#dialogUnitView').css({
        left: "150px",
        top: "250px",
        "z-index" : "900"
    });

    $('#dialog').css({
        left: "150px",
        top: "100px",
        zIndex: 998
    });

    $("#mask").css({
        width: $(window).width() - 150,
        height: $(window).height(),
        position: "absolute",
        top: 0,
        left: "150px",
        opacity: 0,
        backgroundColor: "black",
        'z-index': 999,
        display: 'none'
    });

    $(baseImg).show();
}

function adjustGraphImages(img, baseImg) {
    sizeToFit("#container", img, false);

    var position = $("#img1").offset();
    imageLeft = position.left;
    imageTop = position.top;
    //imageLeft = ( ($('#container').width() - $(baseImg).width()) / 2 ) + 150;
    //imageTop = ($(window).height() - $(baseImg).height()) / 2 + 20;
    //imageTop = 45;

    $(img).css({
        position: "absolute",
        left: imageLeft,
        top: imageTop
    });


}

/**
 * [jgkim] 2015.04.17
 * Ÿ��Ʋ ����
 *
 *
 */
function setTitle() {
    pageURL = this.location.href.substring(this.location.href.lastIndexOf('/') + 1);
    contentID = pageURL.substring(0, pageURL.lastIndexOf('.'));

    for (var i = 0; i < contentsArray.enabledContents.length; i++) {
        if (contentID === contentsArray.enabledContents[i].cd) {
            title = contentsArray.enabledContents[i].tit;
        }
    }
    $("title").html(title);
    $("#titleArea").html(title);

    $('#dialog').draggable({
        containment: "#container",
        iframeFix: true,
        zIndex: 998,
        revert: 'invalid',
        stop : function (event, ui) {
            $(this).draggable('option', 'revert', 'invalid');
        }
    });

    $('.dialog').draggable({
        containment: "#container",
        iframeFix: true,
        zIndex: 998,
        revert: 'invalid',
        stop : function (event, ui) {
            $(this).draggable('option', 'revert', 'invalid');
        }
    });

    $("#container").droppable({

    });

    $('#showDialog').css({
        "width" : "126px",
        "max-width" : "100%",
        "display" : "block",
        "margin-left" : "-12px",
        "margin-top" : "50px",
        "position" : "relative"
    });
}

/**
 * [jgkim] 2015.04.21
 * Ÿ��Ʋ ���?
 *
 */
function toggleTitle() {
    this.src = getToggleImageName(this.src);
    var titleTag = $('#titleArea');
    if(titleTag.text().length > 1) {
        titleTag.text('');
    } else {
        titleTag.text(title);
    }
}

/**
 * [jgkim] 2015.04.17
 * ��ü����(��Ÿ�̾��?) ����
 *
 */
function toggleEntireView() {
    if (this.src.indexOf('toggle') == -1) {
        $('#img3').hide();
    } else {
        initializeGraphImgs();
        if($('#BTN_BOTTOM_MODE').length > 0) {
            $('#BTN_BOTTOM_MODE')[0].src = getToggleOnImageName($('#BTN_BOTTOM_MODE')[0].src);
        }
        $('#img3').show();
    }
    $(this)[0].src = getToggleImageName($(this)[0].src);
    clickCount = 0;
}

/**
 * [jgkim] 2015.04.17
 * ���� ���? ���? ����
 *
 */
function toggleBtnMode() {
    initializeGraphImgs();
    $('#img3').hide();
    $(this)[0].src = getToggleImageName($(this)[0].src);
    $("#BTN_BOTTOM_ALL")[0].src = getToggleOffImageName($("#BTN_BOTTOM_ALL")[0].src);
}

function extractedFunctionFor6021(img) {
    if (this.src.indexOf('toggle') > 0) {
        $(img).hide();
    } else if (this.src.indexOf('toggle') == -1) {
        $('#img3').hide();
        $('.unitView').hide();
        $('.btnUnitViewEX').each(function () {
            $(this)[0].src = getToggleOnImageName($(this)[0].src);
        });
        $(img).show();
    }
}

function toggleOFFTargetButton(target) {
    $(target)[0].src = getToggleOffImageName($(target)[0].src);
}
/**
 * [jgkim] 2015.04.17
 * �����? ���ֺ� ����
 *
 */
function toggleBundleOrUnitView() {
    var id = this.id;

    ///
    /// ���� ���? ��ư�� ���� ��
    ///
    if($('#BTN_BOTTOM_MODE').length > 0) {

        ///
        /// ���? ��
        ///
        if ($('#BTN_BOTTOM_MODE')[0].src.indexOf('toggle') == -1) {

            for (var i = 0; i < CONTENTS_INFO.length; i++) {
                if (id == CONTENTS_INFO[i].btn) {
                    var img = "#" + CONTENTS_INFO[i].imgId;
                    if (this.src.indexOf('toggle') > 0) {
                        $(img).hide();
                    } else if (this.src.indexOf('toggle') == -1) {
                        $('#img3').hide();
                        $(img).show();
                    }
                }
            }
            //$("#BTN_BOTTOM_ALL")[0].src = getToggleOffImageName($("#BTN_BOTTOM_ALL")[0].src);
            toggleOFFTargetButton("#BTN_BOTTOM_ALL");
            $(this)[0].src = getToggleImageName($(this)[0].src);

        ///
        /// ���� ��
        ///
        } else {
            for (var i = 0; i < CONTENTS_INFO_2.length; i++) {
                if (id == CONTENTS_INFO_2[i].btn) {

                    var imgArray = CONTENTS_INFO_2[i].images;
                    var img = imgArray.shift();
                    //console.log(img);
                    imgArray.push(img);

                    if (img == "EMPTY") {
                        for (var j = 0; j < imgArray.length; j++) {
                            var imgHided = "#" + imgArray[j];
                            $(imgHided).hide();
                        }
                    } else {
                        $('#' + img).show();
                    }
                }
            }
        }
    ///
    /// ���� ���? ��ư�� ���� ��
    ///
    }else {

        for (var i = 0; i < CONTENTS_INFO.length; i++) {
            if (id == CONTENTS_INFO[i].btn) {

                ///
                /// [jgkim] 2015.04.20
                /// imgId �� �迭�� �ִ� ���? ó���ϱ� ���� ����.
                /// (6011 �������� ���? ���ֺ信 ������ �ִ�.)
                ///
                if($.isArray(CONTENTS_INFO[i].imgId)) {

                    $('#img3').hide();
                    var imgArray = CONTENTS_INFO[i].imgId;
                    img = "#" + imgArray[clickCount];
                    $(img).show();
                    clickCount++;
                    if(clickCount == imgArray.length + 1) {
                        clickCount = 0;
                        for(var i = 0; i < imgArray.length; i++) {
                            var imgInit = "#" + imgArray[i];
                            $(imgInit).hide();
                        }
                    }

                ///
                /// [jgkim] 2015.04.20
                /// 6021 �������� ���? ���? ���? ��ư�� ������ �����? ���ֺ䰡 ��Ÿ������ �����Ѵ�.
                ///
                }else if(currentURL == "/04-MT000101-1006021.html") {

                    var img = "#" + CONTENTS_INFO[i].imgId;

                    switch (CONTENTS_INFO[i].btn) {

                        case "BTN_BOTTOM_MIDDLE_FIRST" :
                        case ("BTN_BOTTOM_MIDDLE_SECOND") :
                            extractedFunctionFor6021.call(this, img);
                            break;

                        default :
                            if (this.src.indexOf('toggle') > 0) {
                                $(img).hide();
                            } else if (this.src.indexOf('toggle') == -1) {
                                $('#img3').hide();
                                $('.bundleView').hide();
                                $(img).show();
                                $('.btnBundleView').each(function() {
                                    $(this)[0].src = getToggleOnImageName($(this)[0].src);
                                });
                            }
                    }
                    $(this)[0].src = getToggleImageName($(this)[0].src);

                }else {
                    var img = "#" + CONTENTS_INFO[i].imgId;
                    if (this.src.indexOf('toggle') > 0) {
                        $(img).hide();
                    } else if (this.src.indexOf('toggle') == -1) {
                        $('#img3').hide();
                        $(img).show();
                    }
                    $(this)[0].src = getToggleImageName($(this)[0].src);
                }
            }
        }
        toggleOFFTargetButton("#BTN_BOTTOM_ALL");
    }
}

function initializeGraphImgs() {
    $('.unitView').hide();
    $('.bundleView').hide();
    $('.btnUnitView').each(function() {
        $(this)[0].src = getToggleOnImageName($(this)[0].src);
    });
    if($('#BTN_BOTTOM_MODE').length > 0) {
        CONTENTS_INFO_2 = JSON.parse(JSON.stringify(oCONTENTS_INFO_2));
    }
    clickCount = 0;
}

$(window).resize(function () {
    load(baseImg, flagZoom);

    $(".bottomBtn").css('width', $(window).width() * 0.06);          // ���� ��ư��
    $("#BTN_BOTTOM_MODE").css('width', $(window).width() * 0.12);    // ���? ���? ��ư
});

